﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace CheckMonth2
{
    class Program
    {
        static void Main(string[] args)
        {
            const int MAXMONTH = 12;
            const int MINMONTH = 1;

            const int MINDAY = 1;
            const int JANUARY = 31;
            const int FEBRUARY = 29;
            const int MARCH = 31;
            const int APRIL = 30;
            const int MAY = 31;
            const int JUNE = 30;
            const int JULY = 31;
            const int AUGUST = 31;
            const int SEPTEMBER = 30;
            const int OCTOBER = 30;
            const int NOVEMBER = 31;
            const int DECEMBER = 30;

            const string INPUTOOR = "Out of range input.";
            

            int userMonth = 0;
            string userMonthTest = "";
            int userDay = 0;
            string userDayTest;
            bool keepGoing = true;
            string DAYOOR = ("Day is out of range in birth month.");

            do
            {
                Write("\nEnter your birth month (1-12): ");
                userMonthTest = ReadLine();

                if (isNumeric(userMonthTest))
                {
                    userMonth = Convert.ToInt32(userMonthTest);

                    if (userMonth > MAXMONTH || userMonth < MINMONTH)
                    {
                        WriteLine("\n" + INPUTOOR);

                    }

                    else
                    {
                        Write("\nEnter your birth day: ");
                        userDayTest = ReadLine();

                        if (isNumeric(userDayTest))
                        {
                            userDay = Convert.ToInt32(userDayTest);

                            if ((userMonth == 1 && (userDay > JANUARY || userDay < MINDAY)) ||
                                (userMonth == 2 && (userDay > FEBRUARY || userDay < MINDAY)) ||
                                (userMonth == 3 && (userDay > MARCH || userDay < MINDAY)) ||
                                (userMonth == 4 && (userDay > APRIL || userDay < MINDAY)) ||
                                (userMonth == 5 && (userDay > MAY || userDay < MINDAY)) ||
                                (userMonth == 6 && (userDay > JUNE || userDay < MINDAY)) ||
                                (userMonth == 7 && (userDay > JULY || userDay < MINDAY)) ||
                                (userMonth == 8 && (userDay > AUGUST || userDay < MINDAY)) ||
                                (userMonth == 9 && (userDay > SEPTEMBER || userDay < MINDAY)) ||
                                (userMonth == 10 && (userDay > OCTOBER || userDay < MINDAY)) ||
                                (userMonth == 11 && (userDay > NOVEMBER || userDay < MINDAY)) ||
                                (userMonth == 12 && (userDay > DECEMBER || userDay < MINDAY)))
                            {
                                WriteLine("\n" + DAYOOR);
                            }

                            else
                            {
                                keepGoing = false;
                            }

                        }

                        else
                        {
                            Write("\nNon-numeric input.\n");
                        }
                    }
                }



                else
                {
                    Write("\nNon-numeric input.\n");
                }

            } while (keepGoing == true);

            Write("\nYour birthday is " + userMonth.ToString() + "/" + userDay.ToString() + ".");
            ReadLine();
        }



        static private bool isNumeric(String input)
        {
            int test;
            return int.TryParse(input, out test);
        }
    }
}
