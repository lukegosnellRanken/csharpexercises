﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hurricane
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        const int MINMPH = 74;
        const string WINDSPEEDOOR = "Wind speed invalid or out of range input";
        const int CATEGORY5 = 157;
        const int CATEGORY4 = 130;
        const int CATEGORY3 = 111;
        const int CATEGORY2 = 96;
        const int CATEGORY1 = 74;

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            bool keepGoing = true;
            int windSpeed = 0;

            if (keepGoing)
            {
                keepGoing = isNumeric(textBoxWindSpeed.Text);
            }

            if (keepGoing)
            {
                
                windSpeed = Convert.ToInt32(textBoxWindSpeed.Text);

                if (windSpeed < MINMPH)
                {
                    alertMessage(WINDSPEEDOOR, "Invalid input!");
                    clearWindSpeedTextBox();
                    return;
                }
            }

            else
            {
                alertMessage(WINDSPEEDOOR, "Invalid input!");
                clearWindSpeedTextBox();
                return;
            }

            if (keepGoing)
            {
                if (windSpeed >= CATEGORY5)
                {
                    textBoxCategory.Text = "5";
                }
                else if (windSpeed >= CATEGORY4 && windSpeed < CATEGORY5)
                {
                    textBoxCategory.Text = "4";
                }
                else if (windSpeed >= CATEGORY3 && windSpeed < CATEGORY4)
                {
                    textBoxCategory.Text = "3";
                }
                else if (windSpeed >= CATEGORY2 && windSpeed < CATEGORY3)
                {
                    textBoxCategory.Text = "2";
                }
                else
                {
                    textBoxCategory.Text = "1";
                }
            }


        }


        //*****************************************

        private bool isNumeric(string input)
        {
            double test;
            return double.TryParse(input, out test);
        }

        //*****************************************

        private void alertMessage(string msg, string title)
        {
            MessageBox.Show(msg, title,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
        }

        //*****************************************

        private void clearWindSpeedTextBox()
        {
            textBoxWindSpeed.Text = "";
            textBoxWindSpeed.Focus();
        }

        //*****************************************
    }
}
