﻿using System;
using System.Windows.Forms;

/*
 *      Write a program named Admission for a 
 *      college’s admissions office. The user 
 *      enters a numeric high school grade point 
 *      average (for example, 3.2) and an 
 *      admission test score.
 *      
 */

namespace Admission
{
    public partial class FormAdmission : Form
    {
        public FormAdmission()
        {
            InitializeComponent();
        }

        //Declare and initialize progrm constants
        const double MINGPA = 0.0;
        const double MAXGPA = 4.0;
        const int MINADMINTESTSCORE = 0;
        const int MAXADMINTESTSCORE = 100;
        const string GPANONNUMERICOOR = "GPA must be a number between 0.0 and 4.0";
        const string MINADSCORE = "Minimum admission test score: 0";
        const string MAXADSCORE = "Maximum admission test score: 100";
        const string TSTNONNUMERICOOR = "Test score must be a number between 0 and 100";
        const string MINGPASTR = "Must be 0.0 or greater";
        const string MAXGPASTR = "Must be 4.0 or lesser";

        //*****************************************

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            //Declare nd initilize local program variables
            bool keepGoing = true;      //program continue variable
            double gpa = 0.0;
            int testScore = 0;
            
            if (keepGoing)      //same as if (keepGoing == true)
            {
                //Validate the GPA
                keepGoing = isNumeric(textBoxGradePointAverage.Text);
            }

            //GPA was numeric
            if (keepGoing)
            {
                //Convert textbox gpa value to double
                gpa = Convert.ToDouble(textBoxGradePointAverage.Text);

                //Validate that GPA is >= 0.0 and <= 4.0
                if (gpa < MINGPA)
                {
                    alertMessage(GPANONNUMERICOOR, MINGPASTR);
                    clearGPATextBox();
                    return;
                }

                else if (gpa > MAXGPA)
                {
                    alertMessage(GPANONNUMERICOOR, MAXGPASTR);
                    clearGPATextBox();
                    return;
                }

                else     //Valid GPA input
                {
                    //Validate the admission test score
                    keepGoing = isNumeric(textBoxAdmissionTestScore.Text);
                    
                }
            }

            else
            {
                alertMessage(GPANONNUMERICOOR, "INVALID GPA INPUT!");
                clearGPATextBox();
                return;
            }

            if (keepGoing)      //Start the validation of the Admission Test Score
            {
                testScore = Convert.ToInt32(textBoxAdmissionTestScore.Text);

                //Validate the test score
                if (testScore < MINADMINTESTSCORE)  // Inputted test score of < 0
                {
                    alertMessage(TSTNONNUMERICOOR, MINADSCORE);
                    clearAdminTestTextBox();
                    return;
                }

                else if (testScore > MAXADMINTESTSCORE)  // Inputted test score of < 0
                {
                    alertMessage(TSTNONNUMERICOOR, MAXADSCORE);
                    clearAdminTestTextBox();
                    return;
                }

                else     //GPA and Test Score both in range
                {
                    acceptOrRejectApplicant(gpa, testScore);
                }

            }

            else
            {
                alertMessage(TSTNONNUMERICOOR, "Invalid Test Score");
                clearAdminTestTextBox();
                return;
            }

        }

        //*****************************************

        private bool isNumeric(string input)
        {
            double test;
            return double.TryParse(input, out test);
        }

        //*****************************************

        private void alertMessage(string msg, string title)
        {
            MessageBox.Show(msg, title,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
        }

        //*****************************************

        private void clearGPATextBox()
        {
            textBoxGradePointAverage.Text = "";
            textBoxGradePointAverage.Focus();
        }

        //*****************************************

        private void clearAdminTestTextBox()
        {
            textBoxAdmissionTestScore.Text = "";
            textBoxAdmissionTestScore.Focus();
        }

        //*****************************************

        private void acceptOrRejectApplicant(double g,
                                             int ts)
        {
            /*
             *      Display the message Accept if the student 
             *      meets either of the following requirements:
             *      
             *      •	A grade point average >= 3.0 and an
             *          admission test score of >= 60 
             *          
             *      •	A grade point average of < 3.0, and an
             *          admission test score of >= 80 
             *          
             *      •	If the student does not meet either of 
             *          the qualification criteria, display Reject. 
             */

            string status = "";

            if (((g > 3.0) && (ts >= 60)) ||
                ((g < 3.0) && (ts >= 80)))
            {
                status = "Accept";
            }

            else
            {
                status = "Reject";
            }

            textBoxAdmissionStatus.Text = status;

        }

        //*****************************************

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBoxGradePointAverage.Text = "";
            textBoxAdmissionTestScore.Text = "";
            textBoxAdmissionStatus.Text = "";
            textBoxGradePointAverage.Focus();
        }

        //*****************************************

        private void buttonExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Exit the program now?",
                "EXIT PROGRAM???",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) ==
                DialogResult.Yes)

            {
                Application.Exit();
            }
        }
    }
}