﻿namespace Admission
{
    partial class FormAdmission
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelGradePointAverage = new System.Windows.Forms.Label();
            this.labelAdmissionTestScore = new System.Windows.Forms.Label();
            this.labelAdmissionStatus = new System.Windows.Forms.Label();
            this.textBoxGradePointAverage = new System.Windows.Forms.TextBox();
            this.textBoxAdmissionTestScore = new System.Windows.Forms.TextBox();
            this.textBoxAdmissionStatus = new System.Windows.Forms.TextBox();
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelGradePointAverage
            // 
            this.labelGradePointAverage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.labelGradePointAverage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGradePointAverage.Location = new System.Drawing.Point(84, 44);
            this.labelGradePointAverage.Name = "labelGradePointAverage";
            this.labelGradePointAverage.Size = new System.Drawing.Size(227, 23);
            this.labelGradePointAverage.TabIndex = 6;
            this.labelGradePointAverage.Text = "Grade Point Average:";
            this.labelGradePointAverage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelAdmissionTestScore
            // 
            this.labelAdmissionTestScore.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.labelAdmissionTestScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAdmissionTestScore.Location = new System.Drawing.Point(84, 98);
            this.labelAdmissionTestScore.Name = "labelAdmissionTestScore";
            this.labelAdmissionTestScore.Size = new System.Drawing.Size(227, 23);
            this.labelAdmissionTestScore.TabIndex = 7;
            this.labelAdmissionTestScore.Text = "Admission Test Score:";
            this.labelAdmissionTestScore.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelAdmissionStatus
            // 
            this.labelAdmissionStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.labelAdmissionStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAdmissionStatus.Location = new System.Drawing.Point(84, 152);
            this.labelAdmissionStatus.Name = "labelAdmissionStatus";
            this.labelAdmissionStatus.Size = new System.Drawing.Size(227, 25);
            this.labelAdmissionStatus.TabIndex = 8;
            this.labelAdmissionStatus.Text = "Admission Status:";
            this.labelAdmissionStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxGradePointAverage
            // 
            this.textBoxGradePointAverage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxGradePointAverage.Location = new System.Drawing.Point(326, 44);
            this.textBoxGradePointAverage.Name = "textBoxGradePointAverage";
            this.textBoxGradePointAverage.Size = new System.Drawing.Size(222, 26);
            this.textBoxGradePointAverage.TabIndex = 0;
            this.textBoxGradePointAverage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxAdmissionTestScore
            // 
            this.textBoxAdmissionTestScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAdmissionTestScore.Location = new System.Drawing.Point(326, 98);
            this.textBoxAdmissionTestScore.Name = "textBoxAdmissionTestScore";
            this.textBoxAdmissionTestScore.Size = new System.Drawing.Size(222, 26);
            this.textBoxAdmissionTestScore.TabIndex = 1;
            this.textBoxAdmissionTestScore.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxAdmissionStatus
            // 
            this.textBoxAdmissionStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAdmissionStatus.Location = new System.Drawing.Point(326, 154);
            this.textBoxAdmissionStatus.Name = "textBoxAdmissionStatus";
            this.textBoxAdmissionStatus.ReadOnly = true;
            this.textBoxAdmissionStatus.Size = new System.Drawing.Size(222, 26);
            this.textBoxAdmissionStatus.TabIndex = 5;
            this.textBoxAdmissionStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCalculate.Location = new System.Drawing.Point(88, 230);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(119, 56);
            this.buttonCalculate.TabIndex = 2;
            this.buttonCalculate.Text = "Calculate";
            this.buttonCalculate.UseVisualStyleBackColor = true;
            this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClear.Location = new System.Drawing.Point(262, 230);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(119, 56);
            this.buttonClear.TabIndex = 3;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExit.Location = new System.Drawing.Point(429, 230);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(119, 56);
            this.buttonExit.TabIndex = 4;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // FormAdmission
            // 
            this.AcceptButton = this.buttonCalculate;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.CancelButton = this.buttonClear;
            this.ClientSize = new System.Drawing.Size(615, 351);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonCalculate);
            this.Controls.Add(this.textBoxAdmissionStatus);
            this.Controls.Add(this.textBoxAdmissionTestScore);
            this.Controls.Add(this.textBoxGradePointAverage);
            this.Controls.Add(this.labelAdmissionStatus);
            this.Controls.Add(this.labelAdmissionTestScore);
            this.Controls.Add(this.labelGradePointAverage);
            this.Name = "FormAdmission";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Jeff Scott - Chapter 4 Page 181 Admission Program";
            
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelGradePointAverage;
        private System.Windows.Forms.Label labelAdmissionTestScore;
        private System.Windows.Forms.Label labelAdmissionStatus;
        private System.Windows.Forms.TextBox textBoxGradePointAverage;
        private System.Windows.Forms.TextBox textBoxAdmissionTestScore;
        private System.Windows.Forms.TextBox textBoxAdmissionStatus;
        private System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonExit;
    }
}

