﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SumInts
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        
        const int SENTINEL = 999;
        const string INVALID = "Input must be an integer";
        int total = 0;

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            int input = 0;
            bool keepGoing = true;

            if (keepGoing)
            {
                if (isNumeric(textBoxInteger.Text))
                {
                    input = Convert.ToInt32(textBoxInteger.Text);

                    if (input == 999)
                    {
                        textBoxTotal.Text = total.ToString();
                        total = 0;
                        return;
                    }
                    else
                    {
                        total += input;
                    }
                }
                else
                {
                    alertMessage(INVALID, "inputed is not numeric");
                    clearAll();
                    return;
                }
            }


        }

        private void buttonClearAll_Click(object sender, EventArgs e)
        {
            clearAll();
        }


        //*********************************

        private void clearAll()
        {
            textBoxInteger.Text = "";
            textBoxTotal.Text = "";
            textBoxInteger.Focus();
        }

        //*********************************

        private bool isNumeric(String input)
        {
            double test;
            return double.TryParse(input, out test);
        }

        //*********************************

        private void alertMessage(string msg, string title)
        {
            MessageBox.Show(msg, title,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
            textBoxInteger.Text = "";
        }

        //*********************************
    }
}
