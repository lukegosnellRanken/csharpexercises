﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace DisplayMultiplicationTable
{
    class Program
    {
        static void Main(string[] args)
        {
            int i, k, p;
            for (i = 1; i <= 10; i++)
            {
                for (k = 1; k <= 10; k++)
                {
                    p = i * k;

                    Write("   " + p + "   ");
                }

                WriteLine(" ");
            }

            ReadLine();
        }
    }
}
