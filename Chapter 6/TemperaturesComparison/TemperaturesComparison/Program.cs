﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace TemperaturesComparison
{
    class Program
    {

        const int MINTEMP = -20;
        const int MAXTEMP = 130;
        const string GW = "Getting warmer";
        const string GC = "Getting cooler";
        const string MB = "It's a mixed bag";

        static int[] tempsArray = new int[5];


        static void Main(string[] args)
        {
            bool again = true;
            bool warmFlag = false;
            bool coolFlag = false;
            string againStr = "";
            string firstChar = "";

            while (again)
            {
                Console.Clear();
                inputTemperatures();
                warmFlag = warmCheck();
                coolFlag = coolCheck();
                determineWhatToWrite(warmFlag, coolFlag);

                Write("\nRun Program Again? (Y/N)");
                againStr = ReadLine();
                firstChar = againStr.Substring(0, 1);

                if ((firstChar != "Y") && (firstChar != "y"))
                {
                    again = false;
                }
            }
        }

        static void inputTemperatures()
        {
            bool numTest = false;
            String numStr = "";
            Random random = new Random();
            for (int lcv = 0; lcv < tempsArray.Length; ++lcv)
            {
                Write("\nEnter temperature " + (lcv + 1) + " between " + MINTEMP + " and " + MAXTEMP + ": ");
                numStr = ReadLine();
                numTest = isNumeric(numStr);

                if (numTest)
                {
                    tempsArray[lcv] = Convert.ToInt32(numStr);
                }
                else
                {
                    WriteLine("\nEvidently you cannot enter a temperature between " + MINTEMP + " and " + MAXTEMP + " so I will generate one for you!\n");
                    tempsArray[lcv] = random.Next(1, 101);
                }
            }
        }

        static bool isNumeric(String input)
        {
            int test;
            return int.TryParse(input, out test);
        }

        static bool warmCheck()
        {
            for (int lcv = 0; lcv < tempsArray.Length - 1; ++lcv)
            {
                if (tempsArray[lcv + 1] > tempsArray[lcv])
                {
                    continue;
                }
                else
                {
                    return false;
                }
            }

            return true;

        }

        static bool coolCheck()
        {
            for (int lcv = 0; lcv < tempsArray.Length - 1; ++lcv)
            {
                if (tempsArray[lcv + 1] < tempsArray[lcv])
                {
                    continue;
                }
                else
                {
                    return false;
                }
            }

            return true;

        }

        static void determineWhatToWrite(bool wf, bool cf)
        {
            if (wf)
            {
                WriteLine("\n" + GW);
            }
            else if (cf)
            {
                WriteLine("\n" + GC);
            }
            else
            {
                WriteLine("\n" + MB);
            }

            ReadLine();
        }
    }
}
