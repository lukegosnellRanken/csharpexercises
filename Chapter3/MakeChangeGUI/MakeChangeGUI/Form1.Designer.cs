﻿namespace MakeChangeGUI
{
    partial class MakeChangeGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.labelTotal = new System.Windows.Forms.Label();
            this.labelTwenties = new System.Windows.Forms.Label();
            this.labelTens = new System.Windows.Forms.Label();
            this.labelFives = new System.Windows.Forms.Label();
            this.labelOnes = new System.Windows.Forms.Label();
            this.textBoxTotal = new System.Windows.Forms.TextBox();
            this.textBoxTwenties = new System.Windows.Forms.TextBox();
            this.textBoxTens = new System.Windows.Forms.TextBox();
            this.textBoxFives = new System.Windows.Forms.TextBox();
            this.textBoxOnes = new System.Windows.Forms.TextBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.Location = new System.Drawing.Point(209, 369);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(381, 48);
            this.buttonCalculate.TabIndex = 0;
            this.buttonCalculate.Text = "Calculate";
            this.buttonCalculate.UseVisualStyleBackColor = true;
            this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(196, 66);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(128, 13);
            this.labelTotal.TabIndex = 1;
            this.labelTotal.Text = "Enter Total Dollar Amount";
            // 
            // labelTwenties
            // 
            this.labelTwenties.AutoSize = true;
            this.labelTwenties.Location = new System.Drawing.Point(274, 127);
            this.labelTwenties.Name = "labelTwenties";
            this.labelTwenties.Size = new System.Drawing.Size(50, 13);
            this.labelTwenties.TabIndex = 2;
            this.labelTwenties.Text = "Twenties";
            // 
            // labelTens
            // 
            this.labelTens.AutoSize = true;
            this.labelTens.Location = new System.Drawing.Point(293, 179);
            this.labelTens.Name = "labelTens";
            this.labelTens.Size = new System.Drawing.Size(31, 13);
            this.labelTens.TabIndex = 3;
            this.labelTens.Text = "Tens";
            // 
            // labelFives
            // 
            this.labelFives.AutoSize = true;
            this.labelFives.Location = new System.Drawing.Point(292, 237);
            this.labelFives.Name = "labelFives";
            this.labelFives.Size = new System.Drawing.Size(32, 13);
            this.labelFives.TabIndex = 4;
            this.labelFives.Text = "Fives";
            // 
            // labelOnes
            // 
            this.labelOnes.AutoSize = true;
            this.labelOnes.Location = new System.Drawing.Point(292, 296);
            this.labelOnes.Name = "labelOnes";
            this.labelOnes.Size = new System.Drawing.Size(32, 13);
            this.labelOnes.TabIndex = 5;
            this.labelOnes.Text = "Ones";
            // 
            // textBoxTotal
            // 
            this.textBoxTotal.Location = new System.Drawing.Point(354, 63);
            this.textBoxTotal.Name = "textBoxTotal";
            this.textBoxTotal.Size = new System.Drawing.Size(149, 20);
            this.textBoxTotal.TabIndex = 6;
            this.textBoxTotal.TextChanged += new System.EventHandler(this.textBoxTotal_TextChanged);
            // 
            // textBoxTwenties
            // 
            this.textBoxTwenties.Location = new System.Drawing.Point(354, 120);
            this.textBoxTwenties.Name = "textBoxTwenties";
            this.textBoxTwenties.ReadOnly = true;
            this.textBoxTwenties.Size = new System.Drawing.Size(149, 20);
            this.textBoxTwenties.TabIndex = 7;
            // 
            // textBoxTens
            // 
            this.textBoxTens.Location = new System.Drawing.Point(354, 176);
            this.textBoxTens.Name = "textBoxTens";
            this.textBoxTens.ReadOnly = true;
            this.textBoxTens.Size = new System.Drawing.Size(149, 20);
            this.textBoxTens.TabIndex = 8;
            // 
            // textBoxFives
            // 
            this.textBoxFives.Location = new System.Drawing.Point(354, 234);
            this.textBoxFives.Name = "textBoxFives";
            this.textBoxFives.ReadOnly = true;
            this.textBoxFives.Size = new System.Drawing.Size(149, 20);
            this.textBoxFives.TabIndex = 9;
            // 
            // textBoxOnes
            // 
            this.textBoxOnes.Location = new System.Drawing.Point(354, 293);
            this.textBoxOnes.Name = "textBoxOnes";
            this.textBoxOnes.Size = new System.Drawing.Size(149, 20);
            this.textBoxOnes.TabIndex = 10;
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(100, 369);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(103, 48);
            this.buttonClear.TabIndex = 11;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(596, 369);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(103, 48);
            this.buttonExit.TabIndex = 12;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // MakeChangeGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.textBoxOnes);
            this.Controls.Add(this.textBoxFives);
            this.Controls.Add(this.textBoxTens);
            this.Controls.Add(this.textBoxTwenties);
            this.Controls.Add(this.textBoxTotal);
            this.Controls.Add(this.labelOnes);
            this.Controls.Add(this.labelFives);
            this.Controls.Add(this.labelTens);
            this.Controls.Add(this.labelTwenties);
            this.Controls.Add(this.labelTotal);
            this.Controls.Add(this.buttonCalculate);
            this.Name = "MakeChangeGUI";
            this.Text = "MakeChangeGUI";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label labelTwenties;
        private System.Windows.Forms.Label labelTens;
        private System.Windows.Forms.Label labelFives;
        private System.Windows.Forms.Label labelOnes;
        private System.Windows.Forms.TextBox textBoxTotal;
        private System.Windows.Forms.TextBox textBoxTwenties;
        private System.Windows.Forms.TextBox textBoxTens;
        private System.Windows.Forms.TextBox textBoxFives;
        private System.Windows.Forms.TextBox textBoxOnes;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonExit;
    }
}

