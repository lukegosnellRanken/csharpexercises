﻿namespace TestsInteractiveGUI
{
    partial class TestsInteractiveGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.labelTestScore1 = new System.Windows.Forms.Label();
            this.labelAverageTestScore = new System.Windows.Forms.Label();
            this.textBoxTestScore1 = new System.Windows.Forms.TextBox();
            this.textBoxAverage = new System.Windows.Forms.TextBox();
            this.labelTestScore2 = new System.Windows.Forms.Label();
            this.labelTestScore3 = new System.Windows.Forms.Label();
            this.labelTestScore4 = new System.Windows.Forms.Label();
            this.labelTestScore5 = new System.Windows.Forms.Label();
            this.textBoxTestScore2 = new System.Windows.Forms.TextBox();
            this.textBoxTestScore3 = new System.Windows.Forms.TextBox();
            this.textBoxTestScore4 = new System.Windows.Forms.TextBox();
            this.textBoxTestScore5 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.Location = new System.Drawing.Point(178, 334);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(409, 56);
            this.buttonCalculate.TabIndex = 0;
            this.buttonCalculate.Text = "Calculate";
            this.buttonCalculate.UseVisualStyleBackColor = true;
            this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
            // 
            // labelTestScore1
            // 
            this.labelTestScore1.AutoSize = true;
            this.labelTestScore1.Location = new System.Drawing.Point(281, 92);
            this.labelTestScore1.Name = "labelTestScore1";
            this.labelTestScore1.Size = new System.Drawing.Size(68, 13);
            this.labelTestScore1.TabIndex = 1;
            this.labelTestScore1.Text = "Test Score 1";
            // 
            // labelAverageTestScore
            // 
            this.labelAverageTestScore.AutoSize = true;
            this.labelAverageTestScore.Location = new System.Drawing.Point(247, 282);
            this.labelAverageTestScore.Name = "labelAverageTestScore";
            this.labelAverageTestScore.Size = new System.Drawing.Size(102, 13);
            this.labelAverageTestScore.TabIndex = 2;
            this.labelAverageTestScore.Text = "Average Test Score";
            this.labelAverageTestScore.Click += new System.EventHandler(this.label2_Click);
            // 
            // textBoxTestScore1
            // 
            this.textBoxTestScore1.Location = new System.Drawing.Point(385, 89);
            this.textBoxTestScore1.Name = "textBoxTestScore1";
            this.textBoxTestScore1.Size = new System.Drawing.Size(132, 20);
            this.textBoxTestScore1.TabIndex = 3;
            // 
            // textBoxAverage
            // 
            this.textBoxAverage.Location = new System.Drawing.Point(385, 279);
            this.textBoxAverage.Name = "textBoxAverage";
            this.textBoxAverage.ReadOnly = true;
            this.textBoxAverage.Size = new System.Drawing.Size(132, 20);
            this.textBoxAverage.TabIndex = 4;
            // 
            // labelTestScore2
            // 
            this.labelTestScore2.AutoSize = true;
            this.labelTestScore2.Location = new System.Drawing.Point(281, 128);
            this.labelTestScore2.Name = "labelTestScore2";
            this.labelTestScore2.Size = new System.Drawing.Size(68, 13);
            this.labelTestScore2.TabIndex = 5;
            this.labelTestScore2.Text = "Test Score 2";
            // 
            // labelTestScore3
            // 
            this.labelTestScore3.AutoSize = true;
            this.labelTestScore3.Location = new System.Drawing.Point(281, 169);
            this.labelTestScore3.Name = "labelTestScore3";
            this.labelTestScore3.Size = new System.Drawing.Size(68, 13);
            this.labelTestScore3.TabIndex = 6;
            this.labelTestScore3.Text = "Test Score 3";
            // 
            // labelTestScore4
            // 
            this.labelTestScore4.AutoSize = true;
            this.labelTestScore4.Location = new System.Drawing.Point(281, 207);
            this.labelTestScore4.Name = "labelTestScore4";
            this.labelTestScore4.Size = new System.Drawing.Size(68, 13);
            this.labelTestScore4.TabIndex = 7;
            this.labelTestScore4.Text = "Test Score 4";
            // 
            // labelTestScore5
            // 
            this.labelTestScore5.AutoSize = true;
            this.labelTestScore5.Location = new System.Drawing.Point(281, 245);
            this.labelTestScore5.Name = "labelTestScore5";
            this.labelTestScore5.Size = new System.Drawing.Size(68, 13);
            this.labelTestScore5.TabIndex = 8;
            this.labelTestScore5.Text = "Test Score 5";
            // 
            // textBoxTestScore2
            // 
            this.textBoxTestScore2.Location = new System.Drawing.Point(385, 128);
            this.textBoxTestScore2.Name = "textBoxTestScore2";
            this.textBoxTestScore2.Size = new System.Drawing.Size(132, 20);
            this.textBoxTestScore2.TabIndex = 9;
            // 
            // textBoxTestScore3
            // 
            this.textBoxTestScore3.Location = new System.Drawing.Point(385, 166);
            this.textBoxTestScore3.Name = "textBoxTestScore3";
            this.textBoxTestScore3.Size = new System.Drawing.Size(132, 20);
            this.textBoxTestScore3.TabIndex = 10;
            // 
            // textBoxTestScore4
            // 
            this.textBoxTestScore4.Location = new System.Drawing.Point(385, 204);
            this.textBoxTestScore4.Name = "textBoxTestScore4";
            this.textBoxTestScore4.Size = new System.Drawing.Size(132, 20);
            this.textBoxTestScore4.TabIndex = 11;
            // 
            // textBoxTestScore5
            // 
            this.textBoxTestScore5.Location = new System.Drawing.Point(385, 242);
            this.textBoxTestScore5.Name = "textBoxTestScore5";
            this.textBoxTestScore5.Size = new System.Drawing.Size(132, 20);
            this.textBoxTestScore5.TabIndex = 12;
            // 
            // TestsInteractiveGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.textBoxTestScore5);
            this.Controls.Add(this.textBoxTestScore4);
            this.Controls.Add(this.textBoxTestScore3);
            this.Controls.Add(this.textBoxTestScore2);
            this.Controls.Add(this.labelTestScore5);
            this.Controls.Add(this.labelTestScore4);
            this.Controls.Add(this.labelTestScore3);
            this.Controls.Add(this.labelTestScore2);
            this.Controls.Add(this.textBoxAverage);
            this.Controls.Add(this.textBoxTestScore1);
            this.Controls.Add(this.labelAverageTestScore);
            this.Controls.Add(this.labelTestScore1);
            this.Controls.Add(this.buttonCalculate);
            this.Name = "TestsInteractiveGUI";
            this.Text = "TestsInteractiveGUI";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.Label labelTestScore1;
        private System.Windows.Forms.Label labelAverageTestScore;
        private System.Windows.Forms.TextBox textBoxTestScore1;
        private System.Windows.Forms.TextBox textBoxAverage;
        private System.Windows.Forms.Label labelTestScore2;
        private System.Windows.Forms.Label labelTestScore3;
        private System.Windows.Forms.Label labelTestScore4;
        private System.Windows.Forms.Label labelTestScore5;
        private System.Windows.Forms.TextBox textBoxTestScore2;
        private System.Windows.Forms.TextBox textBoxTestScore3;
        private System.Windows.Forms.TextBox textBoxTestScore4;
        private System.Windows.Forms.TextBox textBoxTestScore5;
    }
}

