﻿namespace InchesToCentimetersGUI
{
    partial class FormInchesToCentimetersGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelInches = new System.Windows.Forms.Label();
            this.labelCentimeters = new System.Windows.Forms.Label();
            this.textBoxInches = new System.Windows.Forms.TextBox();
            this.textBoxCentimeters = new System.Windows.Forms.TextBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelInches
            // 
            this.labelInches.BackColor = System.Drawing.Color.Moccasin;
            this.labelInches.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInches.Location = new System.Drawing.Point(115, 106);
            this.labelInches.Name = "labelInches";
            this.labelInches.Size = new System.Drawing.Size(172, 28);
            this.labelInches.TabIndex = 5;
            this.labelInches.Text = "Inches";
            this.labelInches.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCentimeters
            // 
            this.labelCentimeters.BackColor = System.Drawing.Color.Moccasin;
            this.labelCentimeters.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCentimeters.Location = new System.Drawing.Point(115, 252);
            this.labelCentimeters.Name = "labelCentimeters";
            this.labelCentimeters.Size = new System.Drawing.Size(172, 28);
            this.labelCentimeters.TabIndex = 6;
            this.labelCentimeters.Text = "Centimeters";
            this.labelCentimeters.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxInches
            // 
            this.textBoxInches.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxInches.Location = new System.Drawing.Point(333, 107);
            this.textBoxInches.Name = "textBoxInches";
            this.textBoxInches.Size = new System.Drawing.Size(221, 28);
            this.textBoxInches.TabIndex = 0;
            this.textBoxInches.TextChanged += new System.EventHandler(this.textBoxInches_TextChanged);
            // 
            // textBoxCentimeters
            // 
            this.textBoxCentimeters.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCentimeters.Location = new System.Drawing.Point(333, 253);
            this.textBoxCentimeters.Name = "textBoxCentimeters";
            this.textBoxCentimeters.ReadOnly = true;
            this.textBoxCentimeters.Size = new System.Drawing.Size(221, 28);
            this.textBoxCentimeters.TabIndex = 4;
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(625, 155);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(75, 23);
            this.buttonClear.TabIndex = 2;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExit.Location = new System.Drawing.Point(625, 224);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(75, 23);
            this.buttonExit.TabIndex = 3;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.Font = new System.Drawing.Font("Modern No. 20", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCalculate.Location = new System.Drawing.Point(119, 335);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(581, 79);
            this.buttonCalculate.TabIndex = 1;
            this.buttonCalculate.Text = "Calculate";
            this.buttonCalculate.UseVisualStyleBackColor = true;
            this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
            // 
            // FormInchesToCentimetersGUI
            // 
            this.AcceptButton = this.buttonCalculate;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Orange;
            this.CancelButton = this.buttonExit;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonCalculate);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.textBoxCentimeters);
            this.Controls.Add(this.textBoxInches);
            this.Controls.Add(this.labelCentimeters);
            this.Controls.Add(this.labelInches);
            this.Name = "FormInchesToCentimetersGUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Luke Gosnell InchsToCentimetersGUI";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelInches;
        private System.Windows.Forms.Label labelCentimeters;
        private System.Windows.Forms.TextBox textBoxInches;
        private System.Windows.Forms.TextBox textBoxCentimeters;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonCalculate;
    }
}

