﻿using System;
using static System.Console;

namespace StopSign
{
    /*letter H*/

    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("\n\n\n");
            WriteLine("\t\t\t  XXXXXX");
            WriteLine("\t\t\t X\tX");
            WriteLine("\t\t\tX  STOP  X");
            WriteLine("\t\t\t X\tX");
            WriteLine("\t\t\t  XXXXXX");
            WriteLine("\t\t\t     X");
            WriteLine("\t\t\t    X");
            WriteLine("\t\t\t     X");
            WriteLine("\t\t\t    X");
            WriteLine("\t\t\t     X");
            WriteLine("\t\t\t    X");
            WriteLine("\t\t\t     X");
            ReadLine();
        }
    }
}
