﻿using System;
using static System.Console;

namespace InchesToCentimetersInteractive
{
    class Program
    {
        static void Main(string[] args)
        {
            const double CENTIMETERSINANINCH = 2.54;
            double inches = 0.0;
            double centimeters = 0.0;

            Write("Enter a number representing Inches: ");
            inches = Convert.ToDouble(ReadLine());

            centimeters = inches * CENTIMETERSINANINCH;
            WriteLine("\n\n\n\t\t" + inches.ToString() + " inches is " + centimeters.ToString() + " centimeters");

            ReadLine();
        }
    }
}
