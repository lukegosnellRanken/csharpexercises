﻿using System;
using static System.Console;

namespace ProjectedRaises
{
    class Program
    {
        static void Main(string[] args)
        {
            const double RAISE = .04;
            double billySalary = 100000.00;
            double bobSalary = 20000.00;
            double joeSalary = 50000.00;

         
            WriteLine("\n\n\n\t\t Billy's salary: " + billySalary.ToString("C"));
            WriteLine("\n\t\t Bob's's salary: " + bobSalary.ToString("C"));
            WriteLine("\n\t\t Joe's salary: " + joeSalary.ToString("C"));

            billySalary = (billySalary + (billySalary * RAISE));
            bobSalary = (bobSalary + (bobSalary * RAISE));
            joeSalary = (joeSalary + (joeSalary * RAISE));
            WriteLine("\n\n\n\t\t Billy's new salary: " + billySalary.ToString("C"));
            WriteLine("\n\t\t Bob's's new salary: " + bobSalary.ToString("C"));
            WriteLine("\n\t\t Joe's new salary: " + joeSalary.ToString("C"));

            ReadLine();
        }
    }
}
